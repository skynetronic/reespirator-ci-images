#!/bin/bash

IMAGE_NAME=$(grep -m 1 DOCKER_LOCAL_IMAGE_NAME Makefile | sed 's/^.*= //g')
IMAGE_TAG=$(grep -m 1 DOCKER_LOCAL_IMAGE_TAG Makefile | sed 's/^.*= //g')

xhost +

unset DISPLAY
export DISPLAY=:0

docker run  -e DISPLAY --device=/dev/ttyUSB0 --entrypoint arduino -v /tmp/.X11-unix:/tmp/.X11-unix reespirator-ci-arduino

