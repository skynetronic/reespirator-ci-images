

DOCKER_LOCAL_IMAGE_NAME := reespirator-ci-arduino
DOCKER_LOCAL_IMAGE_TAG := latest
DOCKER_LOCAL_IMAGE := $(DOCKER_LOCAL_IMAGE_NAME):$(DOCKER_LOCAL_IMAGE_TAG)

RELEASE_DATE := $(shell date -u +%Y%m%dT%H%M%SZ)
GIT_HASH := $(shell git rev-parse --short HEAD)
RELEASE_VERSION ?= $(RELEASE_DATE)_$(GIT_HASH)

CI_REGISTRY := ${CI_REGISTRY}
CI_REGISTRY_USER := ${CI_REGISTRY_USER}
CI_REGISTRY_PASSWORD := ${CI_REGISTRY_PASSWORD}

DOCKER_REMOTE_IMAGE_NAME := ${CI_REGISTRY_IMAGE}
DOCKER_REMOTE_IMAGE_TAG := $(RELEASE_VERSION)
DOCKER_IMAGE := $(DOCKER_REMOTE_IMAGE_NAME)

LOG := @sh -c 'printf "\e[1;33m"; echo "=> $$@"; printf "\e[0m"' MESSAGE


. PHONY: default build shell login tag push
default: shell

clean:
	${LOG} "Removing local docker image..."
	docker image rm $(DOCKER_LOCAL_IMAGE)

build:
	${LOG} "Building local docker image..."
	docker image build -t $(DOCKER_LOCAL_IMAGE) .

shell: build
	${LOG} "Starting shell on local docker image..."
	docker container run --rm -it $(DOCKER_LOCAL_IMAGE)


login:
	${LOG} "Login into $(CI_REGISTRY) docker respository"
	docker login -u $(CI_REGISTRY_USER) -p $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)

tag:
	${LOG} "Tagging Docker image..."
	docker image tag $(DOCKER_LOCAL_IMAGE) $(DOCKER_IMAGE):$(RELEASE_VERSION)
	docker image tag $(DOCKER_LOCAL_IMAGE) $(DOCKER_IMAGE):latest

push: login tag
	${LOG} "Pushing Docker image release '$(RELEASE_VERSION)'..."
	docker image push $(DOCKER_IMAGE):$(RELEASE_VERSION)
	docker image push $(DOCKER_IMAGE):latest
