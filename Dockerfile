FROM debian:10

RUN apt-get update -qqy && apt-get install -qqy \
  wget ca-certificates make g++ gcc-avr \
  arduino-mk \
  x11-xserver-utils x11-apps \
  openjdk-11-jdk \
  && rm -rf /var/cache/apt/*

# Arduino version and location to install
ENV ARDUINO_IDE_VERSION=1.8.12
ENV ARDUINO_DIR=/opt/arduino-${ARDUINO_IDE_VERSION}

RUN wget -q -O- https://downloads.arduino.cc/arduino-${ARDUINO_IDE_VERSION}-linux64.tar.xz | tar xJ -C /opt \
  && ln -s ${ARDUINO_DIR}/arduino /usr/local/bin/ \
  && ln -s ${ARDUINO_DIR}/arduino-builder /usr/local/bin/ \
  && ln -s ${ARDUINO_DIR} /opt/arduino

# Export data for the Arduino-Makefile (arduino-mk) https://github.com/sudar/Arduino-Makefile
ENV ARDMK_DIR=/usr/share/arduino

RUN mkdir /app
WORKDIR /app

CMD /bin/bash
