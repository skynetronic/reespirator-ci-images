# Docker CI images for respirator project

Docker CI images for respirator project: https://gitlab.com/coronavirusmakers/reespirator

# SetUp entorno de desarrollo local

La imágen de docker del repositorio puede utilizarse como entorno de desarrollo local y conectar directamente con las placas Arduino
mediante la conexión USB del Host en el que se ejecuta dicha imágen.

Es necesario preparar el equipo de desarrollo que hará de host ya que, de lo contrario, no habrá posibilidad física de comunicación serial desde el contenedor hacia nuestro Arduino.
Si desarrollais habitualmente para Arduino ya tendréis resuelta la conectividad con el puerto USB para vuestro  usuario. Si no, bastará con que le
concedais permisos de acceso al puerto USB que utilice el IDE (Arduino IDE). No obstante, el repositorio cuenta con un manual muy básico 
acerca de cómo hacerlo  en docs/manual_entorno_desarrollo.odt


